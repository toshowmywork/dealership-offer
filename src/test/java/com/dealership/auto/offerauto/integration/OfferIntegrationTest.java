package com.dealership.auto.offerauto.integration;

import com.dealership.auto.offerauto.dao.entity.OfferEntity;
import com.dealership.auto.offerauto.dao.repository.IOfferRepository;
import com.dealership.auto.offerauto.properties.AutoDataProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.spec.internal.HttpStatus;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.UUID;

import static com.dealership.auto.offerauto.util.SourceReader.getResourceAsString;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 8081)
class OfferIntegrationTest {


    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    AutoDataProperties autoDataProperties;

    @Autowired
    IOfferRepository offerRepository;

    @BeforeEach
    void setUp(){
        offerRepository.deleteAll();
    }

    @ParameterizedTest
    @CsvSource({
            "integration/select/offer-car-to-select.json, integration/select/car-to-select.json, integration/select/offer-to-inserted.json",
            "integration/select/offer-bike-to-select.json, integration/select/bike-to-select.json, integration/select/offer-to-inserted.json",
            "integration/select/offer-bus-to-select.json, integration/select/bus-to-select.json, integration/select/offer-to-inserted.json",
            "integration/select/offer-moto-to-select.json, integration/select/moto-to-select.json, integration/select/offer-to-inserted.json"
    })
    void selectOfferAuto(String pathOffer, String pathAuto, String pathOfferInserted) throws Exception {
        UUID autoId = UUID.randomUUID();
        UUID offerId = UUID.randomUUID();
        String autoAsString = getSpecificJson(pathAuto,
                                        Map.of("$autoId",autoId.toString()));
        String offerAsString = getSpecificJson(pathOffer,
                                        Map.of("$auto", autoAsString, "$offerId",offerId.toString()));
        String offerInsertedAsString = getSpecificJson(pathOfferInserted,
                                        Map.of("$autoId", autoId.toString(), "$offerId",offerId.toString()));

        OfferEntity insertedOffer = objectMapper.readValue(offerInsertedAsString, OfferEntity.class);
        offerRepository.save(insertedOffer);

        stubFor(WireMock.get(urlEqualTo(autoDataProperties.getSelect() + "/" + autoId))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody(autoAsString)));

        mockMvc.perform(get("/offer/" + offerId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(offerAsString));
    }


    private String getSpecificJson(String pathOffer, Map<String, String> replaces) throws IOException, URISyntaxException {
        String json = getResourceAsString(pathOffer);
        if (replaces != null) {
            for(Map.Entry<String, String> entry : replaces.entrySet()){
                json = json.replace(entry.getKey(), entry.getValue());
            }
        }
        return json;
    }

}
