package com.dealership.auto.offerauto.config;

import com.dealership.auto.offerauto.dao.dto.AutoDto;
import com.dealership.auto.offerauto.dao.dto.BikeDto;
import com.dealership.auto.offerauto.dao.dto.BusDto;
import com.dealership.auto.offerauto.dao.dto.CarDto;
import com.dealership.auto.offerauto.dao.dto.MotoDto;
import com.dealership.auto.offerauto.model.auto.Auto;
import com.dealership.auto.offerauto.model.auto.Bike;
import com.dealership.auto.offerauto.model.auto.Bus;
import com.dealership.auto.offerauto.model.auto.Car;
import com.dealership.auto.offerauto.model.auto.Moto;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        setAutoMapperConfig(modelMapper);
        return modelMapper;
    }

    private void setAutoMapperConfig(ModelMapper modelMapper) {
        modelMapper.createTypeMap(CarDto.class, Car.class)
                .include(Auto.class);
        modelMapper.createTypeMap(BusDto.class, Bus.class)
                .include(Auto.class);
        modelMapper.createTypeMap(BikeDto.class, Bike.class)
                .include(Auto.class);
        modelMapper.createTypeMap(MotoDto.class, Moto.class)
                .include(Auto.class);

        modelMapper.createTypeMap(Car.class, CarDto.class)
                .include(AutoDto.class);
        modelMapper.createTypeMap(Bus.class, CarDto.class)
                .include(AutoDto.class);
        modelMapper.createTypeMap(Bike.class, CarDto.class)
                .include(AutoDto.class);
        modelMapper.createTypeMap(Moto.class, CarDto.class)
                .include(AutoDto.class);
    }
}
