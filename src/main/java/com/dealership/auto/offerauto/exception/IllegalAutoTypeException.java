package com.dealership.auto.offerauto.exception;

public class IllegalAutoTypeException extends RuntimeException{

    public IllegalAutoTypeException(String message){
        super(message);
    }
}
