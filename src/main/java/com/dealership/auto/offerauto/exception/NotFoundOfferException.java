package com.dealership.auto.offerauto.exception;

import lombok.Getter;

import java.util.UUID;

@Getter
public class NotFoundOfferException extends RuntimeException {

    private final UUID id;

    public NotFoundOfferException(UUID id){
        super();
        this.id = id;
    }
}
