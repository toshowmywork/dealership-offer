package com.dealership.auto.offerauto.exception.handler;

import com.dealership.auto.offerauto.exception.NotFoundOfferException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class RestControllerExceptionHandler {

    @ExceptionHandler(value = {NotFoundOfferException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String notfoundAutoException(NotFoundOfferException ex) {
        log.error("Auto not Found: {}", ex.getId());
        return "Auto not Found: " + ex.getId();
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public String resourceNotFoundException(Exception ex) {
        log.error("Internal server error: {}", ex.getMessage(), ex);
        return "Internal server error";
    }

}
