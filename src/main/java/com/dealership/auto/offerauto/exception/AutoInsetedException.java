package com.dealership.auto.offerauto.exception;

public class AutoInsetedException extends RuntimeException{

    public AutoInsetedException(Exception e){
        super(e.getMessage(),e.getCause());
    }

}
