package com.dealership.auto.offerauto.service.util;

import com.dealership.auto.offerauto.exception.IllegalAutoTypeException;
import com.dealership.auto.offerauto.model.auto.Auto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CalculateTaxes implements ICalculateTaxes {

    private final List<ITaxesCalculator> listTaxesCalculators;

    public BigDecimal calculate(Auto auto, BigDecimal price) {
        ITaxesCalculator taxesCalculator = getTaxesCalculator(auto);
        return taxesCalculator.calculateTaxes(price);
    }

    public ITaxesCalculator getTaxesCalculator(Auto auto) {
        return listTaxesCalculators.stream()
                .filter(ct -> ct.support(auto.getType()))
                .findFirst()
                .orElseThrow(() -> new IllegalAutoTypeException("Not found taxes calculator for this type of auto"));
    }
}
