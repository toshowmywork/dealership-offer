package com.dealership.auto.offerauto.service;

import com.dealership.auto.offerauto.dao.IOfferDao;
import com.dealership.auto.offerauto.model.Offer;
import com.dealership.auto.offerauto.service.util.ICalculateTaxes;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OfferService implements IOfferService {

    private final IOfferDao offerDao;
    private final ICalculateTaxes calculateTaxes;

    public Offer selectOneOffer(UUID id) {
        Offer offerSelected = offerDao.findOffer(id);
        BigDecimal calculateTaxes = this.calculateTaxes.calculate(offerSelected.getAuto(), offerSelected.getPrice());
        offerSelected.setTaxes(calculateTaxes);
        return offerSelected;
    }
}
