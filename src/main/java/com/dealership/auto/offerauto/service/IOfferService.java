package com.dealership.auto.offerauto.service;

import com.dealership.auto.offerauto.model.Offer;

import java.util.UUID;

public interface IOfferService {
    Offer selectOneOffer(UUID id);
}
