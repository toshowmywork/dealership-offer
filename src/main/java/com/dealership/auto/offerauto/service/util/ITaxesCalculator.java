package com.dealership.auto.offerauto.service.util;

import com.dealership.auto.offerauto.util.AutoEnum;

import java.math.BigDecimal;

public interface ITaxesCalculator {
    BigDecimal calculateTaxes(BigDecimal price);
    Boolean support(AutoEnum auto);
}
