package com.dealership.auto.offerauto.service.util;

import com.dealership.auto.offerauto.properties.OfferProperties;
import com.dealership.auto.offerauto.util.AutoEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class TaxesCalculatorBus implements ITaxesCalculator {

    private final OfferProperties offerProperties;

    public BigDecimal calculateTaxes(BigDecimal price) {
        return offerProperties.getBusTaxes().divide(BigDecimal.valueOf(100l)).multiply(price);
    }

    public Boolean support(AutoEnum auto) {
        return AutoEnum.BUS.equals(auto);
    }
}
