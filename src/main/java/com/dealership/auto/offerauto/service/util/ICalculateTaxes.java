package com.dealership.auto.offerauto.service.util;

import com.dealership.auto.offerauto.exception.IllegalAutoTypeException;
import com.dealership.auto.offerauto.model.auto.Auto;

import java.math.BigDecimal;

public interface ICalculateTaxes {
    BigDecimal calculate(Auto auto, BigDecimal price);
    ITaxesCalculator getTaxesCalculator(Auto auto);
}
