package com.dealership.auto.offerauto.controller;

import com.dealership.auto.offerauto.model.Offer;
import com.dealership.auto.offerauto.service.IOfferService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("offer")
@RequiredArgsConstructor
public class OfferController {

    private final IOfferService offerService;

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Offer selectOne(@PathVariable UUID id){
        return offerService.selectOneOffer(id);
    }
}
