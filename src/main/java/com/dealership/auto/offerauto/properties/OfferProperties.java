package com.dealership.auto.offerauto.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.math.BigDecimal;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "offer")
@PropertySource(value = "classpath:offer.properties")
public class OfferProperties {

    private BigDecimal bikeTaxes;
    private BigDecimal busTaxes;
    private BigDecimal carTaxes;
    private BigDecimal motoTaxes;
    private BigDecimal ecoTaxes;
}
