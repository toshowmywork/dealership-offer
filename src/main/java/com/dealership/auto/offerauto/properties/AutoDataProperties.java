package com.dealership.auto.offerauto.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "auto.data")
@PropertySource(value = "classpath:auto-data.properties")
public class AutoDataProperties {

    private String url;
    private String select;
}
