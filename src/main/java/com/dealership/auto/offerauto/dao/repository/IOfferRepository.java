package com.dealership.auto.offerauto.dao.repository;

import com.dealership.auto.offerauto.dao.entity.OfferEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface IOfferRepository extends JpaRepository<OfferEntity, UUID> {
}
