package com.dealership.auto.offerauto.dao;

import com.dealership.auto.offerauto.dao.dto.AutoDto;
import com.dealership.auto.offerauto.dao.entity.OfferEntity;
import com.dealership.auto.offerauto.dao.repository.IAutoFeignClient;
import com.dealership.auto.offerauto.dao.repository.IOfferRepository;
import com.dealership.auto.offerauto.exception.NotFoundOfferException;
import com.dealership.auto.offerauto.model.Offer;
import com.dealership.auto.offerauto.model.auto.Auto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class OfferDao implements IOfferDao {

    private final IOfferRepository offerRepository;
    private final IAutoFeignClient autoFeignClient;
    private final ModelMapper modelMapper;

    public Offer findOffer(UUID id) {
        OfferEntity offertSelectedEntity = offerRepository.findById(id).orElseThrow(() -> new NotFoundOfferException(id));
        AutoDto autoSelectedDto = autoFeignClient.findById(offertSelectedEntity.getAutoId());

        return mapOfferDaoDataToOfferModelData(offertSelectedEntity, autoSelectedDto);
    }

    private Offer mapOfferDaoDataToOfferModelData(OfferEntity offertSelectedEntity, AutoDto autoSelectedDto) {
        Offer offerSelected = modelMapper.map(offertSelectedEntity, Offer.class);
        Auto autoSelected = modelMapper.map(autoSelectedDto, Auto.class);
        offerSelected.setAuto(autoSelected);
        return offerSelected;
    }
}
