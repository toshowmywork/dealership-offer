package com.dealership.auto.offerauto.dao.repository;

import com.dealership.auto.offerauto.dao.dto.AutoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient(name = "auto", path = "/auto", url = "${feign.client.config.auto.url}")
public interface IAutoFeignClient {

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    AutoDto findById(@PathVariable UUID id);
}
