package com.dealership.auto.offerauto.dao.dto;

import com.dealership.auto.offerauto.util.AutoEnum;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        visible = true,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CarDto.class, name = "CAR"),
        @JsonSubTypes.Type(value = MotoDto.class, name = "MOTO"),
        @JsonSubTypes.Type(value = BusDto.class, name = "BUS"),
        @JsonSubTypes.Type(value = BikeDto.class, name = "BIKE")
})
@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@NoArgsConstructor
public class AutoDto {

    private UUID id;
    private AutoEnum type;
    private String brand;
    private String model;

}
