package com.dealership.auto.offerauto.dao;

import com.dealership.auto.offerauto.model.Offer;

import java.util.UUID;

public interface IOfferDao {
    Offer findOffer(UUID id);
}
