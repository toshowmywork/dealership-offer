package com.dealership.auto.offerauto.model.auto;

import com.dealership.auto.offerauto.util.AutoEnum;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@NoArgsConstructor
public class Auto {

    private UUID id;
    private AutoEnum type;
    private String brand;
    private String model;

}
