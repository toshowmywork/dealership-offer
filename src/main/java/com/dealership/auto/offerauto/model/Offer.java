package com.dealership.auto.offerauto.model;

import com.dealership.auto.offerauto.model.auto.Auto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Offer {

    private UUID id;
    private Auto auto;
    private BigDecimal price;
    private String description;
    private BigDecimal taxes;

}